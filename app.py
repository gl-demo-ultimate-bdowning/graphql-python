from graphene import ObjectType, String, List, Int, Schema
from flask import Flask, jsonify
from flask_graphql import GraphQLView

# Define the ingredient object type
class Ingredient(ObjectType):
    name = String()
    quantity = Int()

# Sample data for cake ingredients
cake_ingredients = [
    {"name": "Flour", "quantity": 2},
    {"name": "Sugar", "quantity": 1},
    {"name": "Eggs", "quantity": 3},
    {"name": "Butter", "quantity": 1},
    {"name": "Vanilla Extract", "quantity": 1},
    {"name": "Baking Powder", "quantity": 1},
]

# Define the Query object type
class Query(ObjectType):
    get_cake_ingredients = List(Ingredient)

    def resolve_get_cake_ingredients(self, info):
        return [Ingredient(name=ingredient["name"], quantity=ingredient["quantity"]) for ingredient in cake_ingredients]

# Create the schema and bind it to Flask
schema = Schema(query=Query)
app = Flask(__name__)
app.add_url_rule('/graphql', view_func=GraphQLView.as_view('graphql', schema=schema, graphiql=True))

if __name__ == '__main__':
    app.run(debug=True)
