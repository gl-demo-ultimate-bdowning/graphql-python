import requests
from graphene import ObjectType, String, Int, List, Schema, Field

# Define the Ingredient object type
class Ingredient(ObjectType):
    name = String()
    quantity = Int()

# Define the Query object type
class Query(ObjectType):
    get_cake_ingredients = List(Ingredient)

    def resolve_get_cake_ingredients(self, info):
        url = 'http://localhost:5000/graphql'  # Replace with the actual GraphQL API endpoint
        query = '{ getCakeIngredients { name, quantity } }'
        response = requests.post(url, json={'query': query})

        if response.status_code == 200:
            data = response.json().get('data', {})
            ingredients_data = data.get('getCakeIngredients', [])
            return [Ingredient(name=item['name'], quantity=item['quantity']) for item in ingredients_data]
        else:
            print(f"Error: {response.status_code}")
            return []

# Create the schema and execute the query
schema = Schema(query=Query)

def main():
    result = schema.execute('{ getCakeIngredients { name, quantity } }')
    if result.errors:
        print(f"Error: {result.errors}")
    else:
        print(result.data)

if __name__ == '__main__':
    main()